import * as React from 'react'
// import { StoreContext, stores } from 'stores/stores'

import { Nav, NavItem, NavList } from  '@patternfly/react-core'

export const NavBarComponent = ({ modules }) => {
  console.log('modules: %o', modules)

  // See https://github.com/BilalBouk/reactstrap-basic-sidebar
  const moduleEntries = modules.modules.map(module => {
    return ( 
      <NavItem itemId={module.id}>
        {module.id}
      </NavItem>
    )
    // return (
    //   <NavItem key={module.id}>
    //     <NavLink to={module.rootpath}>
    //       {module.id}
    //     </NavLink>
    //   </NavItem>
    // )
  })

  return (
    <Nav variant="vertical" aria-label="Nav">
      <NavList>
        {moduleEntries}
      </NavList>
    </Nav>
  )
}

export default NavBarComponent
