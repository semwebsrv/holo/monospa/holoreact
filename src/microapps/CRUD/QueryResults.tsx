import * as React from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import { Table, 
         TableHeader, 
         TableBody } from '@patternfly/react-table';
import CRUDStore from './CRUDStore'

import _ from 'lodash';

interface QueryResultsProps {
  crudStore: CRUDStore;
}

// This implementation is TABLE based query results - at some point we may have alternate
// presenations such as card based results
const QueryResults = ({crudStore}:QueryResultsProps) => {

  const store = useStore()
  console.log('Stores: %o %o', store, crudStore)

  var column_headings : any[]= [];
  var table_data: any[] = [];

  if ( crudStore.domainConfig != null ) {
    column_headings = crudStore.domainConfig['tabularResults']['columns']?.map((coldef) => {
      return coldef.label;
    });
  }
  else {
    console.error("domain config is missing tabularResults.columns definition of the results table");
  }


  if ( ( crudStore.resultsPage != null ) && 
       ( crudStore.domainConfig != null ) ) {

    var domain_config = crudStore.domainConfig;

    table_data = crudStore.resultsPage.map((result_object) => {
      var row_data: any[] = []
      domain_config['tabularResults']['columns']?.map((coldef) => {
        // result.push(result_object);
        row_data.push(_.get(result_object,coldef.accessPath));
      })

      // return [ 
      //   result_object['id'],
      //   result_object['shortcode'],
      //   result_object['name']
      // ]
      return row_data;
    })
  }
   

  console.log("Column_headings: %o, table_data",column_headings,table_data);

  return (
    <Table cells={column_headings}
           rows={ table_data }
           aria-label="Sticky Header Table Demo"
           isStickyHeader>
      <TableHeader />
      <TableBody />
    </Table>
  )
}

export default observer(QueryResults)

