import React from 'react'
import { observer } from 'mobx-react'
import { StoreContext } from 'stores/stores'
import { FormattedMessage } from 'react-intl'
// eslint-disable-next-line no-unused-vars
import { HoloAppProps } from '../../common/HoloAppProps'
// import SideBar from '../../components/SideBar'


class HoloApp extends React.Component<HoloAppProps, any> {
  static contextType = StoreContext

  componentDidMount() {
    // this.context.holoAppStore.keycloak.loadUserInfo().then(userInfo => {
    //  console.log("Userinfo: %o",userInfo)
    // })
  }

  // eslint-disable-next-line no-useless-constructor
  constructor(props: HoloAppProps) {
    super(props)
  }

  render() {
    // this.context.holoAppStore.keycloak.loadUserProfile().then(()=>{
    //  console.log("OK")
    // })
    console.log('Render HoloApp %o', this.context)

    return (
      <div>
        <p>
          holoApp
        </p>
        <p>Here is a formatted application::
        <FormattedMessage
          id='app.footer'
          defaultMessage='Welcome to {what}'
          description='Welcome header on app main page'
          values={{ what: 'react-intl' }}
        />
        </p>
      </div>
    )
  }
}

export default observer(HoloApp)
