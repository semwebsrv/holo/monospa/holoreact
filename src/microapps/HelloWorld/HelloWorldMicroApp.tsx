import * as React from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'

const HelloWorldMicroApp = () => {

  const store = useStore()
  console.log('Stores: %o', store)

  return (
    <div>
      <h1>Hello World</h1>
      <p>The most simple microfrontend</p>
    </div>
  )
}

export default observer(HelloWorldMicroApp)

