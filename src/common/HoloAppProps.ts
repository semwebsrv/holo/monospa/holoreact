import { History, Location} from 'history'
import { match } from 'react-router-dom'

export interface HoloAppProps {
  classes?: any
  history?: History
  location?: Location
  match?: match
  holoconfig: any
}
