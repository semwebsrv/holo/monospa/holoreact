import * as React from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import CRUDStore from './CRUDStore'

import {
  Form,
  InputGroup,
  TextInput,
  Button
} from '@patternfly/react-core';

interface QueryBuilderProps {
  crudStore: CRUDStore;
}

const QueryBuilder = ({crudStore}:QueryBuilderProps) => {

  const store = useStore()
  console.log('Stores: holoStore:%o CRUDstore:%o', store, crudStore)

  const handleTextChange = value => {
    console.log("handleTextChange %o %o",value,crudStore);
    crudStore.setQuery( { "luceneQueryString" : value } );
  }

  const handleQuery = () => {
    console.log("handleQuery");
    crudStore.executeQuery();
  }

  var current_query_string = crudStore.query['luceneQueryString']
  if ( current_query_string == null)
    current_query_string = ''

  console.log("Query string: %o",current_query_string);
  // <p>Current value {crudStore.query['luceneQueryString']}</p>

  return (
    <Form>
      <InputGroup>
        <TextInput id="qry" 
                   isRequired type="text" 
                   name="simple-form-name-01" 
                   aria-describedby="simple-form-name-01-helper" 
                   onChange={handleTextChange} />
        <Button onClick={handleQuery}>Search</Button>
      </InputGroup>
    </Form>
  )
}

export default observer(QueryBuilder)

