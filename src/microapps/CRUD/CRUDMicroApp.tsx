import * as React from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import Domains from './Domains'
import DomainHome from './DomainHome'
import { Route, Switch } from 'react-router-dom';

const CRUDMicroApp = () => {

  const store = useStore()
  console.log('Stores: %o', store)

  return (
    <Switch>
      <Route component={Domains} path="/crud" exact />
      <Route component={DomainHome} path="/crud/:domain" exact />
    </Switch>
  )
}

export default observer(CRUDMicroApp)

