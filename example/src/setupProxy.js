// https://www.telerik.com/blogs/dealing-with-cors-in-create-react-app
// https://create-react-app.dev/docs/proxying-api-requests-in-development/

const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = function(app) {

  app.use(
    '/mtcpmgt',
    createProxyMiddleware({
      target: 'http://localhost:8082',
      changeOrigin: true
    })
  )

  app.use(
    '/auth',
    createProxyMiddleware({
      target: 'http://localhost:8080/',
      changeOrigin: true
    })
  )

  app.use(
    '/caspcore',
    createProxyMiddleware({
      target: 'http://localhost:8080/',
      changeOrigin: true
    })
  )

  app.use(
    '/holoCoreService',
    createProxyMiddleware({
      target: 'http://localhost:8083/',
      changeOrigin: true
    })
  )
}

