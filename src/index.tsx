import * as React from 'react'
import { StoreContext, stores } from 'stores/stores'
import SecuredApp from './SecuredApp'
import { IntlProvider } from 'react-intl'
// eslint-disable-next-line no-unused-vars
import { HoloAppProps } from './common/HoloAppProps'

import HelloWorldMicroApp from './microapps/HelloWorld/HelloWorldMicroApp'
import CRUDMicroApp from './microapps/CRUD/CRUDMicroApp'

// This map of core resource types that all holo apps can reference as a basic
// building block
import { core_resource_types } from './resourceTypes/coreResourceTypes'

const HoloAppContainer = ({ holoconfig }: HoloAppProps) => {
  const selectedLocale = 'en'

  console.log('Config: %o', holoconfig)
  const i18nMessages = { one: 'One' }
  // Object.assign(i18n_messages, ...i18n_contexts);

  stores.holoAppStore.configureModules(holoconfig)

  return (
    <IntlProvider
        locale={selectedLocale}
        key={selectedLocale}
        messages={i18nMessages}
     >
      <StoreContext.Provider value={stores}>
        <SecuredApp />
      </StoreContext.Provider>
    </IntlProvider>
  )
}

// Export components we want client libraries to be able to import
export { HoloAppContainer, 
         HelloWorldMicroApp,
         CRUDMicroApp,
         core_resource_types }
