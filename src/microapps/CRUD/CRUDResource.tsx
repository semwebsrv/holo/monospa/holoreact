import * as React from 'react'
import { useStore } from 'stores/stores'
import { Card, CardTitle, CardBody, CardFooter, CardHeader, CardActions } from '@patternfly/react-core';
import { observer } from 'mobx-react'

import TimesIcon from '@patternfly/react-icons/dist/js/icons/times-icon';
import { Button } from '@patternfly/react-core';

const CRUDResource = () => {

  const store = useStore()
  console.log('Stores: %o', store)

  return (
    <Card>
      <CardHeader>
        Other header text
        <CardActions>
          <div className="pf-u-float-right">
            <Button variant="plain" aria-label="Action">
              <TimesIcon />
            </Button>
          </div>
        </CardActions>
      </CardHeader>
      <CardTitle>Some Resource</CardTitle>
      <CardBody>
        Card Body
      </CardBody>
      <CardFooter>Footer</CardFooter>
    </Card>
  )
}

export default observer(CRUDResource)

