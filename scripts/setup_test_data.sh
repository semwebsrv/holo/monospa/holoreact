#!/bin/bash


# env.sh is not checked into git - use it to store your local user/pass as follows
# export HOLO_USER=""
# export HOLO_PASS=""
# export KEYCLOAK_REALM="vanillatest"
# export BASE_URL="http://holoapitest.semweb.co"
# export KEYCLOAK_URL="https://keycloak.semweb.co"
# export CLIENT="holo"

. ./env.sh

# Info about the oidc config of any KC realm can be obtained from
# http://keycloakhost:keycloakport/auth/realms/{REALM}/.well-known/openid-configuration
echo OpenID config can be found at http://$KEYCLOAK_URL/auth/realms/$KEYCLOAK_REALM/.well-known/openid-configuration

# -H 'accept: application/json' -H 'Content-type: application/x-www-form-urlencoded' \
KEYCLOAK_RESPONSE=$(curl -s -X POST -H 'Content-type: application/x-www-form-urlencoded' \
        --data-urlencode "client_id=$CLIENT" \
        --data-urlencode "username=$HOLO_USER" \
        --data-urlencode "password=$HOLO_PASS" \
        --data-urlencode "grant_type=password" \
        "${KEYCLOAK_URL}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/token")

echo response: $KEYCLOAK_RESPONSE


USR_JWT=`echo $KEYCLOAK_RESPONSE | jq -rc '.access_token'`

echo Master User will be $USR_JWT

echo Create test part 1 using graphql mutation

curl "$BASE_URL/holoCoreService/graphql" \
       -H "Authorization: Bearer $USR_JWT" \
       -H "X-TENANT: $KEYCLOAK_REALM" \
       -H "Accept: application/json" \
       -H "Content-Type: application/json" -d '
{
  "query": "mutation($party: PartyInputType) { createParty(party: $party) { id name shortcode parent { id name } } }",
  "variables":{
    "party": {
      "name": "Test Party 1",
      "shortcode": "TP1"
    }
  }
}'
