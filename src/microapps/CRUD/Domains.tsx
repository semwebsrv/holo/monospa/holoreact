import * as React from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import { Gallery, GalleryItem } from '@patternfly/react-core';
import { Card, CardTitle, CardBody, CardFooter } from '@patternfly/react-core';
import { Link } from 'react-router-dom'
import { Button } from '@patternfly/react-core';

const Domains = () => {

  const store = useStore()
  console.log('Stores: %o', store)

  var resource_domains = store.holoAppStore.resourceTypeRegister;
  
  const domain_cards = Object.keys(resource_domains).map(domain_key => {

    var domain_info = resource_domains[domain_key]

    return (
      <GalleryItem key={domain_key}>
        <Card>
          <CardTitle>{domain_info.label}</CardTitle>
          <CardBody>
            <Link to={`/crud/${domain_key}`}><Button type="button">search</Button></Link>
          </CardBody>
          <CardFooter>Footer</CardFooter>
        </Card>
      </GalleryItem>
    )
  });



  return (
    <div>
      <Gallery hasGutter>
        { domain_cards }
      </Gallery>
    </div>
  )
}

export default observer(Domains)

