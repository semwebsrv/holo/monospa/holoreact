import React from 'react'
import { observer } from 'mobx-react'
import { StoreContext } from 'stores/stores'
import { HoloAppProps } from '../../common/HoloAppProps'
import { Gallery, GalleryItem } from '@patternfly/react-core';


class About extends React.Component<HoloAppProps, any> {

  static contextType = StoreContext


  componentDidMount() {
    console.log("Keycloak: %o",this.context.holoAppStore.keycloak);
    console.log("Modules: %o",this.context.holoAppStore.moduleRegister);

    // this.context.holoAppStore.keycloak.loadUserInfo().then(userInfo => {
    //  console.log("Userinfo: %o",userInfo)
    // })
  }

  // eslint-disable-next-line no-useless-constructor
  constructor(props: HoloAppProps) {
    super(props)
  }

  render() {

    let moduleGalleryItems = this.context.holoAppStore.moduleRegister.modules.map(module => {
      console.log("Adding info for module %o",module)
      return (
        <GalleryItem key={module.id}>{module.id}</GalleryItem>
      )
    });

    return (
      <div>
        <p>
          About
        </p>
        <Gallery hasGutter>
          {moduleGalleryItems}
        </Gallery>
      </div>
    )
  }
}

export default observer(About)
