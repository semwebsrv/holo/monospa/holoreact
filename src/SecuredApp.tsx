import React from "react";
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'
import TenantContainer from 'TenantContainer'

/**
 */
const SecuredApp = () => {

  const store = useStore()

  if (store.holoAppStore.initialised === true) {
    return (
      <TenantContainer />
    )
  } else {
    return <p></p>
  }
}

export default observer(SecuredApp)
