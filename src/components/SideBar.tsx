import React from 'react'
import { Nav, NavItem, NavList, PageSidebar } from '@patternfly/react-core'
// import classNames from 'classnames'
import { Link } from 'react-router-dom'

export const SideBar = ({ modules, isNavOpen }) => {
  console.log('modules: %o', modules)

  const moduleEntries = modules.map(module => {
    console.log("Adding module %o",module);
    return (
      <NavItem itemId={module.id} key={module.id}>
        <Link to={module.rootpath}>{module.id}</Link>
      </NavItem>
    )
  })

  const HoloNav = (
     <Nav aria-label="Nav">
        <NavList>
          {moduleEntries}
          <NavItem id="3" itemId={3} isActive={false}><Link to="/welcome">WelcomeTest</Link></NavItem>
        </NavList>
      </Nav>
  )
 
  return (
    // https://www.patternfly.org/v4/components/page#vertical-nav
    <PageSidebar nav={HoloNav} isNavOpen={isNavOpen}/>
  )
}

export default SideBar
