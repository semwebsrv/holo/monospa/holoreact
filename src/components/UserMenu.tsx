import React, {useState} from 'react'
import { useStore } from 'stores/stores'
import { observer } from 'mobx-react'

import {
  Dropdown,
  DropdownItem,
  DropdownToggle
} from '@patternfly/react-core';

import CaretDownIcon from '@patternfly/react-icons/dist/js/icons/caret-down-icon';

const UserMenu = () => {

  const store = useStore()

  const [isOpen, setIsOpen] = useState(false);
  
  const onSelect = () => {
  };

  const onToggle = () => {
    setIsOpen( !isOpen );
  }

  const logout = () => {
    console.log("Logout...");
    store?.holoAppStore?.keycloak?.logout();
  }

  // There seems to be an issue with <Link> elements inside <DropdownItem> so we're going to use router.push('/item')
  // instead
  const about = () => {
    // router.push('/about')
  }

  console.log("Render User Menu -- User object: %o",store.sessionStore.user);
  var user_object = store.sessionStore.user;
  const toggle = <DropdownToggle id="user-menu-dropdown-toggle" onToggle={onToggle} toggleIndicator={CaretDownIcon}>{user_object['preferred_username']}</DropdownToggle>

  // https://zen-swanson-2d3350.netlify.app/components/dropdown/
  const dropdownItems = [
      <DropdownItem key="about" onClick={() =>{about()}}>About</DropdownItem>,
      <DropdownItem key="logout" onClick={() =>{logout()}}>Logout</DropdownItem>
  ];


  return (
   <Dropdown onSelect={onSelect}
             toggle={toggle}
             dropdownItems={dropdownItems}
             isOpen={isOpen}
             isPlain={true}
   />
  )
}

export default observer(UserMenu)
