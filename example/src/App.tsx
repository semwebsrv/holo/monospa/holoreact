import React from 'react'

import { HoloAppContainer, 
         HelloWorldMicroApp,
         CRUDMicroApp,
         core_resource_types } from 'holoreact'

const App = () => {

  // This example app only exposes core resource types, if creating another app, your
  // app would add it's own list of resource types to this register. This needs to be the full
  // list of resource types that a holo app is able to support.
  const aggregated_type_list = {
    ...core_resource_types
  }

  const holo_cfg:any ={
    "modules":[
      {
        "id":"welcome",
        "rootpath":"/welcome",
        "component": HelloWorldMicroApp
      },
      {
        "id":"crud",
        "rootpath":"/crud",
        "component": CRUDMicroApp
      },
      {
        "id":"core",
        "rootpath":"/core"
      }
    ],
    "i18n":{
      "en":{
        "welcome": "Welcome",
        "core": "Core",
      }
    },
    "resourceTypeRegister": aggregated_type_list
  }
  return <HoloAppContainer holoconfig={holo_cfg}/>
}

export default App
