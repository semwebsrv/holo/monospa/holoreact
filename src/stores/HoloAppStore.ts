// @ts-nocheck

import { makeObservable, action, observable } from "mobx"
import axios from 'axios'
import Keycloak, { KeycloakInstance, KeycloakConfig } from 'keycloak-js'

export default class HoloAppStore {

  // The default endpoint for requests
  axios: object

  // The String URL of the base endpoint
  baseURL: string

  // Config loaded from server
  loadedConfig: object

  // The current tenant
  tenant: string

  authenticated: boolean = false

  keycloak: KeycloakInstance | null

  initialised: boolean = false

  moduleRegister: any | null = {}

  // The app can know about resource types - for example "DirectoryEntry" that can be referenced
  // in many different places. This register holds the info that allows the apps to create links, 
  // render full and brief representations, create typedowns for selection, show fully fledged 
  // search forms or edit resources of the given type. This is the core of the CRUD mechanism in
  // holo
  resourceTypeRegister:any | null = {}

  sidebarVisible: boolean = false

  constructor() {
    console.log("SessionStore::constructor");
    this.init({});
    makeObservable(this, {
      authenticated: observable,
      initialised: observable,
      moduleRegister: observable,
      resourceTypeRegister: observable,
      sidebarVisible: observable,
      toggleSidebar: action,
      configureModules: action,
      setInitialised: action,
      setAuthenticated: action
    })
  }


  toggleSidebar() {
    this.sidebarVisible = !this.sidebarVisible
    console.log("ToggleSidebar %o",this.sidebarVisible)
  }

  configureModules(value: any) {
    console.log('configureModules(%o)', value)
    if ( value.modules != null ) {
      this.moduleRegister = value.modules;
    }

    if ( value.resourceTypeRegister != null ) {
      this.resourceTypeRegister = value.resourceTypeRegister;
    }
    else {
      console.error("The provided configuration does not specify any resource types - the CRUD app will be empty");
    }
  }

  setInitialised(value: boolean) {
    console.log('Setting initialised to %o', value)
    this.initialised = value
  }

  setAuthenticated(value: boolean) {
    this.authenticated = value;
    // Get the
    console.log("Setting authenticated: %o",value);
    if ( value ) {
      // Inject the keycloak JWT into the axios client as a header
      // https://www.techynovice.com/setting-up-JWT-token-refresh-mechanism-with-axios/
      console.log("Adding Keycloak JWT into axios headers: %o",this.keycloak.token);
      this.axios.defaults.headers.common['Authorization'] = 'Bearer '+this.keycloak.token;
      this.axios.defaults.headers.common['X-tenant'] = this.tenant;
    }
  }

  /**
   * Init is called from HoloAppContainer defined in index.js as exposed by holocore
   * It is passed application configuration - this config is static for the defined application
   * for example - "CASP - The Common Alert Service Platform" provides a particuluar tenant resolution
   * setup and app name. This app_config should NOT be used for deployment configuration - eg server names
   * Such info should be loaded from /holoconf.json within the init method below. Clear separation
   * static "app" config - passed in, load deployment config dynamically below.
   * @param app_config - The static application config for this particular deployment of the holo ui framework
   */
  async init(app_config) {
    console.log('initialise HoloServerApi app_config=%o', app_config)
    console.log("Fetch /holoreact/holoconf.json from same source as app");

    const owner: HoloAppStore = this

    return await window
      .fetch('/holoreact/holoconf.json', { cache: 'no-cache' })
      .then((response) => response.json())
      .then((config_json) => {
        owner.loadedConfig = config_json
        console.log('Loaded Config: %o %o', owner.loadedConfig)

        owner.baseURL = config_json.serviceBaseUrl;
        if ( owner.baseURL == null ) {
          console.log("serviceBaseURL key not found in holoconf.json file");
        }
        else {
          console.log("Initialse holo base URL tp %s",owner.baseURL);
        }

        owner.axios = axios.create({
          baseURL: owner.baseURL,
          timeout: 3000,
          headers: {
            accept: 'application/json',
            'content-type': 'application/json'
          }
        })

        // Call the tenant resolver function if defined
        if (config_json.tenantResolver === 'static') {
          console.log("Using static tenant resolver");
          return config_json.staticTenant
        } else if (config_json.tenantResolver === 'host') {
          console.log("Call HOST based tenant resolver \"%s\" baseUrl \"%s\"", config_json.tenantResolver, owner.baseURL);
          // TODOTODOTODO
          return this.axios.post(owner.baseURL + '/mtcpmgt/tenantResolver', { "method": "host", "content": window.location.hostname } ).then(function (response) {
            console.log("Response %o",response);
            return response.data.tenant;
          });
        }
        else {
          console.log("Unhandled tenant resolver \"%s\"",config_json.tenantResolver);
          return null
        }
      })
      .then( (tenant_id) => {
        this.tenant = tenant_id;
        console.log("Setting tenant to %s",tenant_id);
        console.log('At end %o', this)
      })
      .then(() => {
        console.log('Initialise keycloak.... %s/%s', this.tenant, this.loadedConfig.keycloakUrl)
        this.keycloak = Keycloak({
          realm: this.tenant,
          clientId: 'holo',
          url: this.loadedConfig.keycloakUrl // was url: 'http://localhost:3000/auth'
        } as KeycloakConfig)
      })
      .then(() => {
        this.setInitialised(true)
        return true
      })
      .catch(function (err) {
        console.log('Problem fetching holoconf.json', err)
        return false
      })
  }

  getAxios() {
    console.log('getAxios....')
    return this.axios
  }

  getTenant() {
    return this.tenant
  }

  // https://medium.com/@enetoOlveda/how-to-use-axios-typescript-like-a-pro-7c882f71e34a

  /**
   * Execute a restful search
   * @param resourceType
   * @param params
   */
  restfulSearch(resourceUriPath, params) {
    const defaultParams = { stats: 'true', perPage: '20' }
    const mergedProps = { ...defaultParams, ...params }
    console.log('make search request with params %o', mergedProps)

    // return this.axios.get(resourceUriPath, mergedProps );
    return this.axios.get(resourceUriPath, { params: mergedProps })
  }

  // Query is a graphql query of the form "query($un: String!) { generalUserQuery(username: $un, max: 100) { totalCount results { id username } } }"
  graphQLoverJSON(graphql_path, p_query, p_variables) {
    const defaultParams = { stats: 'true', max: '20', offset: 0 }
    const mergedProps = { ...defaultParams, ...p_variables }

    console.log(
      'make graphql request to %s query is %s with variables %o,%o',
      graphql_path,
      p_query,
      p_variables,
      mergedProps
    )

    const graphql_request = {
      query: p_query,
      variables: mergedProps
    }

    const result = this.axios.post(this.baseURL + graphql_path, graphql_request)

    console.log(
      'GraphQL %o [tenant %s] response %o',
      graphql_request,
      this.tenant,
      result
    )

    return result
  }

  postResource(resource_type, record) {
    return this.axios.post(this.baseURL + '/' + resource_type, record)
  }

  putResource(resource_type, resource_id, record) {
    return this.axios.put(
      this.baseURL + '/' + resource_type + '/' + resource_id,
      record
    )
  }

  getResource(graphql_path, resourceType, resourceShape, id) {
    console.log('getResource(%s,%s,%s)', resourceType, resourceShape, id)
    // return this.axios.get(resourceUriPath+"/"+id);
    return this.graphQLoverJSON(
      graphql_path,
      'query($id: String!) { ' +
        resourceType +
        '(' +
        id +
        ') ' +
        resourceShape +
        ' }',
      { id: id }
    )
  }

  searchRemoteKb(query, remote_kb_identifier) {
    console.log('searchRemoteKb(%s,%s) %s', query, remote_kb_identifier)
    return this.axios.get('/erm/queryRemoteKb', {
      params: { q: query, target: remote_kb_identifier }
    })
  }

  /**
   * This method causes a graphql mutation over json of the form
   *
   * {
   *   query: "mutation($record: createPersonType) { createPerson(person: $record) { id, name } }",
   *   variables: {
   *     record:{
   *       name : "The person name"
   *     }
   *   }
   * }
   *
   *
   * @param createType - the -GraphQL- type used for the record variable in the request - createPersonType above
   * @param createMutation - the name of the mutation to be used to create the resource - createPerson above
   * @param resourceType - our internal resource type - person in the example above
   * @param resultShape - the shape of JSON we want returned - { id, name } in the example above
   * @param resource_record - the actual record to create
   * @returns {*}
   */
  createResource(
    graphql_path,
    createType,
    createMutation,
    resourceType,
    resultShape,
    resource_record
  ) {
    console.log(
      'HoloServerApi::createResource(%o,%o,%o,%o,%o,%o)',
      graphql_path,
      createType,
      createMutation,
      resourceType,
      resultShape,
      resource_record
    )

    const graphql_variables = {}
    // Our context stores a root element for the object with the key of the type. If we are editing a party record the
    // record will look like this: { party: { id:, name:,.... the create mutation wants to be passed only the inner
    // content of this structure, so we extract it here and pass along only the inner map.
    graphql_variables.record = resource_record[resourceType]
    return this.graphQLoverJSON(
      graphql_path,
      'mutation($record: ' +
        createType +
        '!) { ' +
        createMutation +
        '(' +
        resourceType +
        ' : $record ) ' +
        resultShape +
        ' }',
      graphql_variables
    )
  }

  // getResourceTypeRegister() {
    // var resource_domains = {
    //   'holo.core.directory.DirectoryEntry': {
    //     domain: 'DirectoryEntry',
    //     label: 'crud.domain.DirectoryEntry.two',
    //     resourceType:'HoloGraphql',
    //     resourceUrl:'/graphql',
    //     resultsType:'TABLE',
    //     resultsDefinition: 'DirectoryEntry::TableLayout::Default',  // The Element Set Name to use by default
    //     viewLayouts:{},
    //     editLayouts:{
    //       'DEFAULT':{
    //          type:'component',
    //          componentName:'DirectoryEntryDefaultEditLayout' // Lookup using this key
    //       }
    //     }
    //   },
    //   'holo.core.parties.Party': {
    //     domain: 'Party',
    //     label: 'crud.domain.Party',
    //     resultsType: 'TABLE',
    //     resultsLayout: 'BRIEF'
    //   },
    // }
    // return resource_domains;
    // return this.resourceTypeRegister;
  // }
}
