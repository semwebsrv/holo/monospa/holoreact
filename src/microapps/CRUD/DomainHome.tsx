import * as React from 'react'
import { useStore } from 'stores/stores'
import { useState } from "react"
import { observer } from 'mobx-react'
import QueryBuilder from './QueryBuilder'
import QueryResults from './QueryResults'
import CRUDResource from './CRUDResource'
// import { Split, SplitItem } from '@patternfly/react-core';
import { Grid, GridItem } from '@patternfly/react-core';
import { Stack, StackItem } from '@patternfly/react-core';
import CRUDStore from './CRUDStore'

const DomainHome = (props) => {

  const store = useStore()
  console.log('Stores: %o, Props: %o', store, props)

  var domainConfig = store.holoAppStore.resourceTypeRegister[props.match.params.domain]

  const [ crudData ] = useState( new CRUDStore(domainConfig, store.holoAppStore) );

  console.log("CrudData: %o", crudData);

  // var query = crudData.query;

  console.log("Domain config: %o",domainConfig);

  // Split should be replaced with https://www.patternfly.org/v4/components/drawer#drawer - (Resizable on inline)

  // Domain name is props.match.params.domain

  return (
    <Grid hasGutter>
      <GridItem span={6}>
        <Stack>
          <StackItem>
            <QueryBuilder crudStore={crudData}/>
            Domain : {props.match.params.domain}
          </StackItem>
          <StackItem isFilled>
            <QueryResults crudStore={crudData} />
          </StackItem>
        </Stack>
      </GridItem>
      <GridItem span={6}>
        <CRUDResource />
      </GridItem>
    </Grid>
  )
}

export default observer(DomainHome)

