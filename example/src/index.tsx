import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import './index.scss';
// import './index.css';
// import '@patternfly/patternfly/patternfly.css'

ReactDOM.render(<App />, document.getElementById('root'))
