import DirectoryEntryEdit from './DirectoryEntryEdit'

// the key for this map will be used in URLs for the crud app - it should be URL friendly
// and preferably easy to remember and unique over resource types
const core_resource_types = {
  'dirents': {
    domain: 'DirectoryEntry',
    label: 'crud.domain.DirectoryEntry',
    resourceType:'HoloGraphql',
    resourceUrl:'/graphql',
    resultsType:'TABLE',
    resultsDefinition: 'DirectoryEntry::TableLayout::Default',  // The Element Set Name to use by default
    tabularResults:{
      columns:[
        { label:'ID', accessPath:'id' },
        { label:'Shortcode', accessPath:'shortcode' },
        { label:'Name', accessPath:'name' }
      ]
    },
    viewLayouts:{},
    editLayouts:{
      'DEFAULT':{
        type:'component',
        component: DirectoryEntryEdit
      }
    }
  },
  'parties': {
    domain: 'Party',
    fqdomain: 'holo.core.parties.Party',
    label: 'crud.domain.Party',
    resultsType: 'TABLE',
    resultsLayout: 'BRIEF'
  },
};

// The map of resource types available from the core module.. every app would want to have these basic
// types
export { core_resource_types }
