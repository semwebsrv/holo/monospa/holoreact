import { makeObservable, action, observable } from "mobx"
import HoloAppStore from "../../stores/HoloAppStore"

export default class CRUDStore {

  query : object = {
    luceneQueryString:''
  };

  resultsPage : Array<object> = [];

  domainConfig: null | object = null;

  holoAppStore: null | HoloAppStore = null;

  resultCount: null | bigint;

  /**
   * The intersection of a server config, a config description of a resource domain (Effectively, a domain class in the back end) 
   * and a user search input / result set
   */
  constructor(domainConfig: object, holoAppStore: HoloAppStore) {
    console.log("CRUDStore::constructor");
    this.domainConfig = domainConfig;
    this.holoAppStore = holoAppStore;

    makeObservable(this, {
      query: observable,
      resultsPage: observable,
      setQuery: action,
      setResultsPage: action,
      executeQuery: action,
      resultCount: observable
    })
  }

  setQuery(new_query: object) {
    console.log('setQuery(%o)', new_query)
    this.query = new_query
  }

  setResultsPage(new_results_page: Array<object>) {
    console.log('setResultsPage(%o)', new_results_page)
    this.resultsPage = new_results_page
  }

  executeQuery() {
    console.log("CRUDStore::executeQuery query:%o domainConfig:%o",this.query, this.domainConfig);

    if ( this.holoAppStore != null ) {
      // var query_result = this.holoAppStore.graphQLoverJSON('/holoCoreService/graphql', 
      this.holoAppStore.graphQLoverJSON('/holoCoreService/graphql', 
                                   'query($q: String, $max:Int, $offset:Int) { findPartyByLuceneQuery(luceneQueryString: $q, max:$max, offset:$offset, sort:"name") { totalCount results { id name shortcode fqshortcode } } }', 
                                   {q:'Test', max:10, offset:0})
        .then((response) => {
          console.log("Got query result: %o",response);
          if ( response?.data?.data != null ) {
            var query_results = response.data.data.findPartyByLuceneQuery;
            this.resultCount = query_results.totalCount;
            this.resultsPage = query_results.results;
            
          }
        })
      // console.log("Got query result: %o",query_result);
    }
  }
}

