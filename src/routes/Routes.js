import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute'
import HoloApp from 'views/main/HoloApp';
import About from 'views/main/About';


const Routes = ({modules}) => {

  console.log('modules for routes: %o', modules)
  const moduleRoutes = modules.map(module => {
    console.log("Adding route for module %o",module)
    return (
      <ProtectedRoute key={module.id} component={module.component} path={module.rootpath} />
    )
  });

  return (
    <Switch>
      <Redirect exact from="/" to="/main" />
      <ProtectedRoute component={About} exact path="/about" />
      <ProtectedRoute component={HoloApp} exact path="/main" />
      {moduleRoutes}
      <Redirect to="/notfound" />
    </Switch>
  );
}

export default Routes;

