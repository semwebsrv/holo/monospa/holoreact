# holoreact

When pushed to master, updated npm packages are deployed to the semwebsrv npm registry at https://gitlab.com/api/v4/packages/npm/

These packages are browsable at

    https://gitlab.com/groups/semwebsrv/-/packages



> holoreact

[![NPM](https://img.shields.io/npm/v/holoreact.svg)](https://www.npmjs.com/package/holoreact) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save holoreact
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'holoreact'
import 'holoreact/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

 © [ianibo](https://github.com/ianibo)



Created with

    npx create-react-library --template typescript

Reports back

To get started, in one tab, run:
$ cd holoreact && npm start

And in another tab, run the create-react-app dev server:
$ cd holoreact/example && npm start


# Roadmap

This library is a container component that can be used to construct apps. The structure is as follows:

    <Your App Assembly Project::App.tsx>
      - Sets up a config object that enumerates the modules available in a given deployment
      - Sets up the holoapp.json config file that points the app at a server and a way to resolve tenants
      - Renders 
          <Holo::HoloAppContainer> - passing in the config object (Defined in Holo/src/index.tsx). This component
                                     sets up the mobx providers and i18n for the app anhd 
                                     Calls stores.holoAppStore.configureModules passing in the app config object
                                         - Configure modules will resolve the tenant and perform any pre-setup
                                     then imports <Holo:SecuredApp>
              <Holo:SecuredApp> waits for the app initialisation to be complete (Observing the initialised property) 
                                and then imports <Holo:TenantContainer>
                  <Holo:TenantContainer> - has a useEffect that will invoke the now configured keycloak to do auth and lay out the site


# Keycloak

https://stackoverflow.com/questions/28658735/what-are-keycloaks-oauth2-openid-connect-endpoints

says 

/auth/realms/{realm}/.well-known/openid-configuration is the json doc that contains oauth connect points and other config

also https://scalac.io/user-authentication-keycloak-1/


See 


https://www.patternfly.org/v4/demos/primary-detail
ianibbo@alcubierre:~/dev/semweb/holo/MonoSPA/holoreact$ find ./src/ -type f -exec grep reactstrap {} /dev/null \;
./src/components/SideBar.tsx:import { NavItem, NavLink, Nav } from 'reactstrap'
./src/components/NavBarComponent.tsx:import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap'
./src/components/NavBarComponent.tsx:  // See https://github.com/BilalBouk/reactstrap-basic-sidebar


See https://github.com/patternfly/patternfly-react-seed
    https://dev.to/kenmoini/up-and-running-with-patternfly-4-4akj
    https://www.patternfly.org/v4/components/dropdown
